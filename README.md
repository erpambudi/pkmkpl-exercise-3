# PKMKPL Exercise 3 Test Isolation 

Sumber latihan ini di dapatkan dari buku :

[Test-Driven Development with Python, by Harry Percival O Reilly, 2017](https://www.obeythetestinggoat.com/pages/book.html)

Project Sebelumnya dapat di akses di
[Exercise 2](https://gitlab.com/erpambudi/website-django.git)

Project Django ini adalah sebuah halaman website sederhana dan masih berada di local server.
Untuk melihat halaman website yang sudah di deploy bisa di akses di :
https://erpamweb.herokuapp.com/


Jika website tidak bisa di buka, kemungkinan website dalam mode sleep. Cobalah untuk me refresh website secara berulang.



Di dalam Latihan Exercise 3 kali ini yaitu :
1. Memindahkan functional_tests.py yang terdapat pada root project ke dalam foder test tersendiri.
2. Mengubah fungsi time.sleep pada functional_tests.py
3. Melakukan testing pada database yang berbeda yaitu database pengujian


Untuk struktur project pada Latihan Exercise 3 kali ini dapat di lihat di bawah ini :

```
.
├── functional_tests
│   ├── __init__.py
│   └── tests.py
├── lists
├── superlists
├── manage.py
├── automatic_comment.py
├── chromedriver.exe
├── db.sqlite3
├── geckodriver.log
.
```


Setelah functional_tests.py telah di pindahkan ke dalam foldernya sendiri, 
maka untuk menajalankan testing dengan cara :


> **erpamDjango>python manage.py test** 

Pada exercise sebelumnya saya menggunakan time.sleep untuk mengatur berapa lama testing di jalankan.
Pada exercise kali ini penggunaan time.sleep di ubah agar penggunaannya jadi lebih baik lagi, yaitu dengan cara memodifikasi 
function yang sebelumnya bernama `check_for_row_in_list_table` pada `functional_tests.py` dan sekarang menjadi `wait_for_row_in_list_table` yang berada di `functional_tests/tests.py` kemudian 
mengatur lama maksimal testing ketika terjadi error dan tidak ada error.


 ```
def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:  
            try:
                table = self.browser.find_element_by_id('id_todo_table')  
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:  
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5) 
```



