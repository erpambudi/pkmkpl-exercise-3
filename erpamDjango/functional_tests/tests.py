from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest
from selenium.common.exceptions import WebDriverException

MAX_WAIT = 10

class NewVisitorTest(LiveServerTestCase):  

    def setUp(self):  
        self.browser = webdriver.Chrome()

    def tearDown(self):  
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:  
            try:
                table = self.browser.find_element_by_id('id_todo_table')  
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:  
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5) 

    def test_can_start_a_list_and_retrieve_it_later(self):  
        
        self.browser.get(self.live_server_url)
        
        # functional test, mencari elemen tag h1
        # kemudian mencari text To-Do di dalam element h1 tersebut
        header_text = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn('To-Do', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        
        #unit test, komentar otomatis jika ada item lebih dari 6 di To-Do list
        self.wait_for_row_in_list_table('6: Tugas Technopreneurship')

        inputbox.send_keys('Oh Tidaaaakk banyak sekali tugas kali ini... Bagaimana iniiiiiii')
        inputbox.send_keys(Keys.ENTER)

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish the test!')
